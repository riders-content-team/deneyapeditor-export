import os

from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.responses import FileResponse
from submission import ResultsHandler
from pathlib import Path
import requests
import json
import yaml
import copy

from typing import List
from pydantic import BaseModel


# Starts Api Server
app = FastAPI()
#game_results_handler = ResultsHandler()

allowed_origin = os.getenv("GODOT_PYTHON_URI") if os.getenv("GODOT_PYTHON_URI") else 'http://localhost:8000'#"127.0.0.1:8000"
user_code_uri = os.getenv("USER_CODE_URI") if os.getenv("USER_CODE_URI") else "../user-code"
scenario_path = './../../LightingBinding/ScenarioFiles/'
riders_scenario_path = '/workspace/src/testing/scenarios/'#user_code_uri + "scenario_files/" 

scenario_path = riders_scenario_path if os.getenv("GODOT_PYTHON_URI") else scenario_path

@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    response = await call_next(request)
    response.headers["Cross-Origin-Opener-Policy"] = "same-origin"
    response.headers["Cross-Origin-Embedder-Policy"] = "require-corp"
    response.headers["Cross-Origin-Resource-Policy"] = "cross-origin"
    return response


@app.get("/get-file/{file_name}")
async def get_map(file_name):
    print("get method worked")
    

@app.post("/send-file/")
async def send_map(req:Request):
    print("send_method worked")
    result = await req.json()
    await convert_result(result)
    return {"status":"SUCCESS"}


# Serves static files contained in export folder to client.

# app.mount("/user-code", StaticFiles(directory= user_code_uri), name="user-code-file-server")
if "RIDERS_HOST" in os.environ:
    app.mount("/project", StaticFiles(directory= "/workspace/src/_lib/"), name="project-materials-server")
app.mount("/", StaticFiles(directory= "../godot-web-export"), name="godot-web-file-server")


