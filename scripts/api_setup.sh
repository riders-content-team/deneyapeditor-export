#!/bin/bash

BLUE='\033[0;34m'
NC='\033[0m' # No Color
COLOR="$BLUE"

source "${RIDERS_GLOBAL_PATH}/lib/helpers.sh"
source "${RIDE_PACKAGES_PATH}/deneyapeditor-export/scripts/godot_setup.sh"

ride_ports_expose "$GODOT_PYTHON_PORT" "$GODOT_PYTHON_PORT"

cd "${RIDE_PACKAGES_PATH}/deneyapeditor-export/api" 
python3 -m uvicorn main:app --reload --host 0.0.0.0 --port $GODOT_PYTHON_PORT
#